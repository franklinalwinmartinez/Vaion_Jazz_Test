

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author frank
 */
public class GeneralProgramming {

    public static void main(String[] args) {

        int evenValue = 100;
        BigInteger total = sumFibonacciFirstEven(evenValue, false);
        System.out.println("Total Fibonacci: " + total);
       
        List<Integer> listA = new ArrayList<>();
        listA.add(1);
        listA.add(4);
        listA.add(6);

        List<Integer> listB = new ArrayList<>();
        listB.add(2);
        listB.add(3);
        listB.add(5);

        System.out.println("Sorted list: "+mergebyCollections(listA, listB));
        System.out.println("Sorted list: "+merge(listA, listB));
        
        Map<Integer, String> alfabet = getAlfabet();
        System.out.println("Conversion base: "+getNumberBase(100, 16, alfabet));
    }

    public static Map<Integer, String> getAlfabet() {
        Map<Integer, String> alfabet = new HashMap<>();

        for (int i = 0; i < 10; i++) {
            alfabet.put(i, String.valueOf(i));
        }
        int key = 10;
        for (int i = 65; i < 91; i++) {
            char c = (char) i;
            alfabet.put(key, String.valueOf(c));
            key++;
        }

        return alfabet;
    }

    public static String getNumberBase(int number, int base, Map<Integer, String> alfabet) {
        if (base < 2 || base > 36) {
            throw new IllegalArgumentException("Not allow base. Range base between 2 and 36 ");
        }
        if (number < 0) {
            throw new IllegalArgumentException("Only positive numbers");
        }

        String numberInBase = "";
        int value = number;
        int remainder;
        String remainderSring;
        do {
            remainder = value % base;
            remainderSring = alfabet.get(remainder);
            numberInBase = remainderSring + numberInBase;
            value = value / base;
        } while (value > 0);

        return numberInBase;

    }

    public static List<Integer> mergebyCollections(List<Integer> listA, List<Integer> listB) {
        List<Integer> ListAB = new ArrayList<>();
        ListAB.addAll(listA);
        ListAB.addAll(listB);
        Collections.sort(ListAB);
        return ListAB;
    }

    public static List<Integer> merge(List<Integer> listA, List<Integer> listB) {
        List<Integer> ListAB = new ArrayList<>();
        int numItem = listA.size() + listB.size();
        int a = 0;
        int b = 0;

        for (int i = 0; i < numItem; i++) {
            if (b >= listB.size() || (a < listA.size() && listA.get(a) <= listB.get(b))) {
                ListAB.add(listA.get(a));
                a++;
            } else {
                ListAB.add(listB.get(b));
                b++;

            }
        }
        return ListAB;
    }

    private static BigInteger sumFibonacciFirstEven(int evenValue, boolean verbose) {
        BigInteger f0 = new BigInteger("1");
        BigInteger f1 = new BigInteger("1");
        BigInteger sequenceValue;
        BigInteger total = new BigInteger("0");
        final BigInteger two = new BigInteger("2");
        boolean reach = false;
        int i = 0;
        int sequence = 3;
        while (!reach) {
            sequenceValue = f0.add(f1);
            f0 = f1;
            f1 = sequenceValue;
            if (verbose) {
                System.out.printf(" %d : %s \n", sequence, sequenceValue);
            }
            sequence++;
            if (sequenceValue.remainder(two).equals(BigInteger.ZERO)) {
                total = total.add(sequenceValue);
                i++;
                if (i >= evenValue) {
                    reach = true;
                }
            }
        }
        return total;
    }
    

}
