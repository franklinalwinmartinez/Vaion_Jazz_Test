package test.java.bytes;


import java.util.List;

/**
 *
 * @author frank
 */
public class BytesReverser {

    public void reverseBytes(List<Byte> bytes, boolean verbose) {

        String stringBytes = "";

        stringBytes = bytes.stream().map((byteItem) -> bytesToString(byteItem)).map((stringByte) -> stringByte).reduce(stringBytes, String::concat);

        String cadenaInv = new StringBuilder(stringBytes).reverse().toString();
        
        if (verbose) {
            System.out.println(stringBytes);
            System.out.println(cadenaInv);
        }

        for (int i = 0; i < bytes.size(); i++) {
            String sByte = cadenaInv.substring(i*8, i*8 + 8);
            bytes.set(i, (byte) Integer.parseInt(sByte));
        }

    }

    public  String bytesToString(Byte byteItem) {
        String stringByte = Integer.toBinaryString((byteItem + 256) % 256);
        stringByte = String.format("%8s", stringByte).replaceAll(" ", "0");
        return stringByte;
    }

}
