
def fn(v, lst=[]):
 lst.insert(0,v)
 print(lst)

fn(23)            # Case 1
fn(16,[1, 2])     # Case 2
fn([10], [3, 4])  # Case 3
fn([11])          # Case 4

import numpy as np
def sumEvenNumber( integers):
    integerNp =  np.asarray(integers)
    evenMembers = (integerNp[ integerNp%2 ==0])
    return (evenMembers*evenMembers).sum()
integers = [1,2,3,4,5,6]
sumEvenNumber(integers)
