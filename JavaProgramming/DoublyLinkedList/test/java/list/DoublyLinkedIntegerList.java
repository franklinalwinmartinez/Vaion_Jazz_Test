package test.java.list;

/**
 *
 * @author frank
 */
public class DoublyLinkedIntegerList {

    private int size = 0;
    private IntegerItem initIntegerItem;
    private IntegerItem endIntegerItem;

    public IntegerItem addIntegerAtEnd(int value) {

        size++;
        IntegerItem newItem = new IntegerItem(value);
        if (initIntegerItem == null) {
            initIntegerItem = newItem;
            endIntegerItem = newItem;
        } else {
            endIntegerItem.setRightItem(newItem);
            newItem.setLeftItem(endIntegerItem);
            endIntegerItem = newItem;
        }
        return newItem;
    }

    public boolean containts(IntegerItem integerItem) {
        IntegerItem next = initIntegerItem;

        while (next != null) {
            if (next.equals(integerItem)) {
                return true;
            }
            next = next.getRightItem();
        }
        return false;
    }

    public boolean removeItemByReference(IntegerItem integerItem) {

        if (!containts(integerItem)) {
            throw new IllegalArgumentException("IntegerItem is not in the list");
        }

        if (integerItem != null) {
            size--;
            if (size == 1) {
                initIntegerItem = null;
                endIntegerItem = null;
                return true;
            }
            if (integerItem.getRightItem() != null && integerItem.getLeftItem() == null) {
                integerItem.getRightItem().setLeftItem(null);
                initIntegerItem = integerItem.getRightItem();
                return true;
            }
            if (integerItem.getRightItem() == null && integerItem.getLeftItem() != null) {
                integerItem.getLeftItem().setRightItem(null);
                endIntegerItem = integerItem.getLeftItem();
                return true;
            }
            integerItem.getLeftItem().setRightItem(integerItem.getRightItem());
            integerItem.getRightItem().setLeftItem(integerItem.getLeftItem());
            return true;
        }
        return false;
    }

    public IntegerItem addIntegerAtIndex(int position, int value) {
        IntegerItem newItem = new IntegerItem(value);
        size++;
        if (initIntegerItem == null) {
            initIntegerItem = newItem;
            endIntegerItem = newItem;
        } else {

            IntegerItem nextIntegerItem = initIntegerItem;
            int index = 0;

            while (nextIntegerItem != null && index < position) {
                index++;
                nextIntegerItem = nextIntegerItem.getRightItem();
            }

            if (nextIntegerItem == null) {
                endIntegerItem.setRightItem(newItem);
                newItem.setLeftItem(endIntegerItem);
                endIntegerItem = newItem;
            } else if (nextIntegerItem.getLeftItem() == null) {
                initIntegerItem.setLeftItem(newItem);
                newItem.setRightItem(initIntegerItem);
                initIntegerItem = newItem;
            } else {
                nextIntegerItem.getLeftItem().setRightItem(newItem);
                newItem.setLeftItem(nextIntegerItem.getLeftItem());
                newItem.setRightItem(nextIntegerItem);
                nextIntegerItem.setLeftItem(newItem);
            }
        }
        return newItem;
    }

    public IntegerItem getElementByIndex(int index) {
        
        IntegerItem nextIntegerItem = initIntegerItem;
        int count = 0;

        while (nextIntegerItem != null && count < index) {
            count++;
            nextIntegerItem = nextIntegerItem.getRightItem();
        }
        return nextIntegerItem;

    }

    @Override
    public String toString() {
        String list = "";
        IntegerItem next = initIntegerItem;
        while (next != null) {
            list += String.format("[ %s ]", next.toString());
            next = next.getRightItem();
        }
        return list;
    }

    public int getSize() {
        return size;
    }

}
