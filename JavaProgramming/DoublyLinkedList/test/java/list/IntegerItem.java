
package test.java.list;

/**
 *
 * @author frank
 */
public class IntegerItem 
{
    private Integer value; 
    private IntegerItem leftItem;
    private IntegerItem rightItem; 
            
    public IntegerItem(int value)
    {
        this.value = value; 
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public IntegerItem getLeftItem() {
        return leftItem;
    }

    public void setLeftItem(IntegerItem leftItem) {
        this.leftItem = leftItem;
    }

    public IntegerItem getRightItem() {
        return rightItem;
    }
    
   
    public void setRightItem(IntegerItem rightItem) {
        this.rightItem = rightItem;
    }

    @Override
    public String toString() 
    {   
         String leftItemValue = "";
         String rightItemValue = "";
         
         if(rightItem!=null)
         {
             rightItemValue = rightItem.getValue().toString(); 
         }
         if(leftItem!=null)
         {
             leftItemValue = leftItem.getValue().toString();
         }
         
         return String.format("%s <- %d -> %s",leftItemValue,value  , rightItemValue);
    } 
    
    
}
