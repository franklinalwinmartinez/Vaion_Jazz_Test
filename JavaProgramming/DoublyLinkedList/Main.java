


import test.java.list.DoublyLinkedIntegerList;
import test.java.list.IntegerItem;
/**
 *
 * @author frank
 */
public class Main {
    
     public static void main(String[] args) 
     {
         
         DoublyLinkedIntegerList  doublyLinkedIntegerList = new  DoublyLinkedIntegerList();
         
         IntegerItem integerItem_2 = doublyLinkedIntegerList.addIntegerAtEnd(2);
         IntegerItem integerItem_4 = doublyLinkedIntegerList.addIntegerAtEnd(4);
         IntegerItem integerItem_6 = doublyLinkedIntegerList.addIntegerAtEnd(6);
         IntegerItem integerItem_8 = doublyLinkedIntegerList.addIntegerAtEnd(8);
         IntegerItem integerItem_10 = doublyLinkedIntegerList.addIntegerAtEnd(10);
         
         
         IntegerItem integerItem_1 = doublyLinkedIntegerList.addIntegerAtIndex(0, 1);
         IntegerItem integerItem_3 = doublyLinkedIntegerList.addIntegerAtIndex(2, 3);
     
         System.out.println(doublyLinkedIntegerList);
         
         doublyLinkedIntegerList.removeItemByReference(integerItem_8);
         
         System.out.println(doublyLinkedIntegerList);
         
         System.out.println(doublyLinkedIntegerList.getElementByIndex(4));
     }
    
}
