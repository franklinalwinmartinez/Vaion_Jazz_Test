
import java.util.ArrayList;
import java.util.List;
import test.java.bytes.BytesReverser;


/**
 *
 * @author frank
 */
public class Main {

    public static void main(String[] args) {

        List<Byte> bytes = new ArrayList<>();
        bytes.add((byte) 2);
        bytes.add((byte) 101);
        bytes.add((byte) 10);

        BytesReverser bytesReverser = new BytesReverser();
        bytesReverser.reverseBytes(bytes, true);

        bytes.forEach((B) -> {
            System.out.println(bytesReverser.bytesToString(B));
        });

    }

}
